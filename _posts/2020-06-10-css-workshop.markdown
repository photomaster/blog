---
layout: post
title:  "Ergebnisse des CSS-Workshops"
date:   2020-06-10 14:57:14 +0200
categories: mmt workshop
---
**In diesem Blog-Post wird einerseits näher auf das Master-Projekt [*PhotoMaster*](https://photomaster.rocks/ "Link zum Projekt") und dessen Zielsetzung sowie Zielgruppe eingegangen. Anderseits werden die Erkenntnisse und Ergebnisse eines CSS-Workshops dargestellt und die Website vor und nach diesem Workshop verglichen.**

## Was ist *PhotoMaster*?

![image]({{site.baseurl}}/assets/screenshots/map_startseite.jpg)

Manchmal stellt sich die Auswahl des perfekten Fotomotivs beim Fotografieren als nicht ganz triviale Aufgabe heraus. Eine Anekdote aus meiner persönlichen Erfahrung: Man nimmt sich spontan einen Nachmittag Zeit, um dem eigenen Auto die längst überfällige Portion Liebe entgegenzubringen. Ein paar Stunden später ist der Liebling geputzt, poliert und fertig gewachst. Natürlich möchte man anschließend die Momentaufnahme seines Werks festhalten und vielleicht sogar über diversen Social Media Kanälen mit seinen Freunden teilen. Nur wie finde ich einen interessanten Ort für den perfekten Schnappschuss? Wo lässt sich mein Motiv gut in Szene setzen? Die Auswahl eines Orts für ein Fotoshooting trifft jeden fotointeressierten Menschen früher oder später.

Zielgruppe dieser Anwendung sind Hobby-Fotograf\*innen, welche auf der Suche nach interessanten Orten für Schnappschüsse sind. Die Plattform soll den User\*innen dabei helfen, einen passenden Ort oder generell Inspiration für ein Foto zu finden. PhotoMaster bietet dafür interessante Daten zu jedem Foto, die für die fotointeressierte Benutzer\*innen von Nutzen sind. Dazu zählen der genaue Ort, die Richtung und Neigung des Geräts während der Aufnahme, den Wetterzustand und weitere über öffentliche APIs verfügbare Daten. Für eine spezifische Suche werden die Fotos mit diesen externen Daten angereichert, zum Beispiel Wetter oder Labels, die den Inhalt des Fotos beschreiben. So soll es möglich sein, detaillierter nach Fotos suchen zu können. Beispielsweise könnte man nach “Fluss”, “Schnee” und “Wald” suchen. Haben bereits andere ein solches Foto aufgenommen und geteilt, könnte der Ort für die Suchende/den Suchenden interessant sein. Das soll den Fotograf\*innen helfen, Fotos besser zu verstehen und die eigenen Fähigkeiten zu verbessern. Außerdem können über Fotos, die dem/der User*in gefallen, neue, nicht bekannte Orte in der Umgebung empfohlen werden. Um von Start an ein möglichst breites Grundspektrum an Locations und Fotos anbieten zu können, werden gemeinfreie Fotos aus diversen anderen Online-Portalen importiert und für unsere Zwecke verarbeitet. 

### Wer benutzt *PhotoMaster*?

Es verwundert wohl eher wenig, dass Fotografieren ein beliebtes Hobby ist. Laut einer repräsentativen Umfrage zur Urlaubsgestaltung der BAT Stiftung für Zukunftsfragen aus dem Jahr 2013 gaben mehr als die Hälfte der Befragten Fotografieren als ihre liebste Urlaubsbeschäftigung an, mit deutlichem Abstand zu kulturellen Aktivitäten, Wellness und Sport[^1]. Die Popularität des Smartphones spielt in diesem Zusammenhang eine tragende Rolle: Zwischen 2011 und 2013 wurden laut Schätzungen genausoviele Fotos geschossen, wie seit der Erfindung der Kamera bis 2011[^2]. Über Social Media Plattformen lassen sich Fotos schnell und komfortabel mit dem persönlichen Umkreis teilen. Auf Facebook werden pro Tag mehr als 300 Millionen Fotos veröffentlicht[^2]. Der Trend zum Smartphone zeichnet sich auch in der Anzahl an verkauften Digitalkameras ab: Seit dem Verkauf des ersten iPhones 2007 ist die Stückzahl verkaufter Kameras stetig rückläufig, während immer mehr Smartphone verkauft werden[^3]. Aufgrund dieser Tatsachen wäre in der Theorie jeder Mensch mit einem Smartphone Teil der Zielgruppe dieses Projekts. PhotoMaster möchte aus dieser Gruppe jene ansprechen, die die Fotografie zu ihrem Hobby gemacht haben oder es in der Zukunft vorhaben. Die Plattform möchte die Benutzer\*innen tatkräftig bei der Motiv- und Ortsfindung für Fotos unterstützen. Aus diesen Überlegungen heraus ergeben sich für dieses Projekt drei hauptsächliche Personas:

1. Hobby-Fotograf\*innen:
  - ![image martin](https://images.unsplash.com/photo-1553584548-776d0277af39?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=150&q=80)
  - **Martin Bleibtreu**
  - 24-35 Jahre
  - abgeschlossenes Studium
  - seit dem Studium arbeitet in einem mittelständischen Unternehmen als Leiter einer Versicherungsabteilung
  - Fotografie betreibt er semi-professionell in seiner Freizeit. Sein Hobby hilft ihm dabei zu entspannen und dient als Ausgleich zu seinem stark theoretischen Beruf
  - fortgeschrittene Kenntnisse, sucht Inspiration, will eigene Fähigkeiten verbessern
  - manchmal fotografiert er für Bekannte und seine Familie auf privaten Feiern. Geld verdient möchte er jedoch dabei nicht.
  - ist mit einer handelsüblichen Spiegelreflexkamera ausgestattet
  - für Urlaubsfotos und auf Reisen verwendet er fast ausschließlich sein Smartphone, weil er die Handhabung so praktischer findet
2. Social Media Benutzer\*innen
 - **Vanessa Berger**
 - ![image vanessa](https://images.unsplash.com/photo-1557053910-d9eadeed1c58?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=150&q=80)
 - 18-24 Jahre
 - arbeitet in einer Marketingagentur für Print und Digitales
 - gestaltet Flyer, setzt Texte, organisiert Kampagnen
 - sie betreibt Fotografie vorwiegend für ihre privaten Instagram und Snapchat Kanäle
 - in ihrer Freizeit bloggt sie auf ihrer Webseite zu den Themen Fashion, Ernährung und Reisen
 - das Teilen von Statusupdates kombiniert mit Aufnahmen von Personen oder hippen Locations ist ihr sehr wichtig (Selfies!)
 - legt Wert auf ihre Online-Präsenz
 - dazu nutzt sie ausschließlich ihr Smartphone, weil sich Fotos bequem nach dem Ablichten auf diverse Plattformen hochladen lassen
 - möchte interessante Orte für Selfies finden
 - hohes Geltungsbedürfnis, Eigenwerbung, Follower generieren
3. Wandernde
 - **Helmut Müller**
 - ![image helmut](https://images.unsplash.com/photo-1506435431442-c3494c6d1560?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=250&q=80)
 - 44-55 Jahre
 - als Berufskraftfahrer in einer internationalen Spedition tätig
 - ist berufsbedingt die meiste Zeit in Europa unterwegs
 - schießt gerne Fotos mit dem Smartphone, um seiner Familie zu zeigen, wo er sich auf seinen Reisen gerade befindet. Teilt gerne lustige Aufnahmen
 - in seiner Freizeit geht er gerne wandern, weil er nach seiner Zeit beim Militär seine Naturverbundenheit wiederentdeckt hat
 - ist gerne in den Bergen unterwegs
 - zu Geburtstagen verschenkt er gerne selbsterstellte Fotobücher an seine Bekannten
 - sucht kleinere Wanderwege in der Nähe seines Wohnortes, welche nicht von Touristen überlaufen sind
 - auf der Suche nach "geheimen" Orten

### Hobby-Fotograf\*innen
![image alt <]({{site.baseurl}}/assets/personas/2.png)
Hobby-Fotograf\*innen betreiben die Fotografie neben ihrem Beruf in ihrer Freizeit aus unterschiedlichen Gründen. Häufig besitzen sie mittelmäßige bis gehobene Ausstattung und sind technisch versiert. Diese Gruppe stellt für PhotoMaster die Kerngruppe dar. Die Plattform bietet den Fotograf\*innen Unterstützung bei der kontextbezogenen Auswahl von Orten und Motiven. Durch die Kategorisierung und Anreicherung von Fotos mit Metadaten können passende Orte leichter gefunden werden. Da die Fotograf\*innen selbst Fotos anfertigen, können sie ihr erworbenes Wissen und Erfahrungen später wieder auf PhotoMaster teilen, um der Community einen Teil zurückzugeben und anderer Personen zu inspirieren. Speziell für diese Zielgruppe bietet PhotoMaster zu jedem Foto kontextbezogene Zusatzinformationen, welche relevant sein könnten, um die Entstehung des Fotos besser verstehen zu können.
<div style="clear:both"></div>

### Social Media User / Influencer
![image alt <]({{site.baseurl}}/assets/personas/3.png)
Social Media User\*innen sind auf der Suche nach Inspiration und neuen Orten, um mehr Follower generieren zu können. Durch Postings mit interessanten Hintergründen oder Motiven kann das Interesse der Follower gesteigert werden. Auch die Aufnahme eines ungewöhnlichen Profilbilds kann Motivation sein, um PhotoMaster zu nutzen. Ein weiterer Grund PhotoMaster zu nutzen ist die Möglichkeit, sich auf einer weiteren Plattform positionieren zu können und die Inhalte zu veröffentlichen. Da unsere Plattform aber den Fokus auf Orte und Motive legt und nicht auf einzelne Personen, welche fotografiert werden, stellen sie für uns nur eine Nebengruppe dar. Zudem ist zu erwarten, dass Fotos mit hoher Wahrscheinlichkeit eher in den bereits etablierten sozialen Medien veröffentlicht werden. Trotzdem wollen wir auch Benutzer\*innen von sozialen Medien ermutigen PhotoMaster zu nutzen, um Orte für Fotos zu finden, die dann anderweitig veröffentlicht werden.  
<div style="clear:both"></div>

### Wandernde
![image alt <]({{site.baseurl}}/assets/personas/1.png)
Auch naturbegeisterte Personen können PhotoMaster nutzen, um Wanderwege, Strecken und Sehenswürdigkeiten abseits der populären Wege zu entdecken. Beispielsweise können die bereits hochgeladenen Fotos benutzt werden, um Kapellen im Umland zu finden. Über eine integrierte Umkreissuche lassen sich die Ergebnisse auf eine relevante Teilmenge in Fußreichweite beschränken. Auch diese Personen sind zwar primär nicht direkt Teil der Zielgruppe, können aber durch Teilen ihres Wissens um Wanderwege und andere heimische Sehenswürdigkeiten eine Bereicherung für die PhotoMaster Community darstellen.  
<div style="clear:both"></div>

## CSS-Workshop

Im Folgenden werden die Überlegungen und Ergebnisse des eintätigen CSS-Workshops im Zuge der Lehrveranstaltung *Client Side Web Engineering* des Studiengangs *MultiMediaTechnolgy* an der FH Salzburg dargelegt. 

### Probleme in der UI

Als Vorbereitung für den Workshop wurde im Projekt-Team überlegt, welche Probleme in der UI des Web-Projekts bestehen. Dazu wurde die Website von beiden Teammitgliedern genauestens auf diesen Aspekt untersucht sowie die Notizen zu den bereits durchgeführten User-Testings herangezogen. Dabei sind sie zu den folgenden Erkenntnissen gelangt: 

1. Grundsätzlich ist die Website ohne Probleme benutzbar, das *Accessibility*-Kriterium für gute UIs[^5] wird also erfüllt. Dies bestätigen die bereits erfolgten User-Testings. Keine der Testpersonen hatte Probleme mit der Website zu interagieren. Auch das Upload-Feature, welches aktuell am weitesten entwickelt ist, ist problemlos und ohne zusätzliche Informationen durch das Projektteam verwendbar. Allerdings kommt es vor, dass manche kleinere Details, wie die Möglichkeit das Banner zu editieren, von den User\*innen übersehen werden. Dieses Problem entspricht dem *Guidance & Hierarchy*-Kriterium für gute UIs[^5]: Es ist aktuell nicht immer klar wo die/der User\*in etwas finden kann. 

1. Ein weiteres Problem in der UI besteht darin, dass manche zentrale Funktionen wie beispielsweise die Suchfunktion aktuell sehr versteckt sind. Dadurch können sie nicht so einfach gefunden werden und sind, auch wenn man genau weiß wo sie sich befinden, nur über umständliche Umwege aufrufbar. Dieses Problem ist ebenfalls dem *Guidance & Hierarchy*-Kriterium für gute UIs[^5] zuzuordnen. Daher wäre es ratsam, diese Funktionen zentraler zu platzieren, beispielsweise im Header. Dadurch kann garantiert werden, dass sie von überall und jederzeit einfach aufzurufen sind. 

1. Die visuellen Rückmeldungen des UIs von *PhotoMaster* sind alle tendenziell von negativer Natur. Darunter fallen unter anderem
    * ein animiertes Lade-Icon ("du musst noch warten"),
    * ein Hinweis auf eine falsche Eingabe im Formular ("hier passt etwas nicht"),
    * ein Hinweis auf einen Fehler nach der Formular-Übermittlung ("Username oder Passwort stimmen nicht")

    wohingegen positive Erlebnisse wie ein erfolgreicher Upload eines Fotos visuell nicht unterstrichen werden. Dies würde jedoch die Freude beim Benutzen der Applikation steigern und sollte daher vom Projektteam in Angriff genommen werden. Dieses Problem entspricht sowohl dem *Responsive & Forgiving*- (auf die/den User\*in reagieren und Feedback geben) als auch dem *Beauty & Delight*-Kriterium (Wartezeit verkürzen und visuelle Belohnungen) für gute UIs[^5]. 

1. Zudem sind kleinere visuelle Bugs über das Projekt verstreut. Hier ein springendes Element, da ein etwas zu geringer Abstand und so weiter. Diese ändern aber nichts daran, ob die Website bedienbar ist oder nicht. Außerdem sind die kleineren visuellen Bugs den Test-User\*innen nicht aufgefallen, sondern wurden nur vom Projektteam beim Beobachten der Testpersonen notiert. Daher haben diese UI-Probleme eine geringere Priorität. 

1. Als letzter Punkt ist die stetige Vereinfachung des UIs ein Anliegen des Projektteams. Als Beispiel die Input-Felder zu den Tags eines Fotos: Natürlich funktionieren sie in der aktuellen Umsetzung ohne Probleme, jedoch könnte man das Benutzererlebnis noch weiter verbessern indem man beispielsweise Vorschläge integriert. Anstatt als User\*in also ein eventuell langes Wort eintippen zu müssen (und sich mit Tippfehlern zu plagen) wäre ein Vorschlag basierend auf den Tags von bereits hochgeladenen Fotos eine wesentliche Erleichterung für die/den User\*in. Ein Tippen auf den Bildschirm würde genügen, um den Tag hinzuzufügen. Dieses Beispiel entspricht sowohl dem *Accessibility*- (Bedienung vereinfachen, auch für unterschiedliche Devices) als auch dem *Guidance & Hierarchy*-Kriterium (die/den User\*in lenken und Denkarbeit abnehmen) für gute UIs[^5]. 

### Priorität und Machbarkeit

Alle gefundenen UI-Probleme sind vom Projektteam lösbar, wobei natürlich manche Tasks mehr und manche weniger Zeit in Anspruch nehmen werden. Ursprünglich dachte das Projektteam, es könne die Probleme innerhalb von 1-2 Tagen lösen. Nach Durchführung des Workshops wurde diese Schätzung nach oben korrigiert: Die Behebung der UI-Probleme des Projekts wird voraussichtlich eine Woche in Anspruch nehmen. 

Teilt man die gefundenen Probleme in strukturelle und visuelle Probleme ein so ist deutlich zu sehen, dass fast alle gefundenen Probleme visueller Natur sind. Nur die Umplatzierung von zentralen Funktionen (siehe Punkt 2) ist den strukturellen Problemen zuzuordnen. 

### Umsetzung

Im Zuge des Workshops wurden vom Projektteam die folgenden drei Aspekte bearbeitet:

* die Header-Höhe wird angepasst, sodass sie, sobald man nach unten gescrollt hat, geringer wird
* die Teaser-Boxen auf der Startseite bekommen eine Generalüberholung um eindeutig als klickbar identifiziert zu werden
* eine neue Unterseite, auf der die in den Teaser-Boxen angeschnittenen Features näher erklärt werden

Die jeweiligen Issues im Gitlab des Projekts können in [diesem Meilenstein](https://gitlab.mediacube.at/groups/photo-master/-/milestones/5 "Link zu Gitlab") gefunden werden. 

#### Header

Ursprünglich sah der Header so aus:

![image]({{site.baseurl}}/assets/screenshots/header_alt.png)

Er ist mit seinen 64 Pixel Höhe relativ hoch und kann in manchen Fällen dadurch wichtige Elemente der Seite verdecken, wie hier zum Beispiel die Fehlermeldung, dass die Position des Fotos nicht automatisiert festgestellt werden konnte und daher manuell über die Karte angegeben werden muss. Um diesem Problem zu begegnen und den User\*innen die Nutzung der Seite zu erleichtern wurde entschieden den Header, sobald die/der User\*in etwas nach unten gescrollt hat, kleiner zu machen. Die Ziel-Größen wurden in einem Mockup festgehalten:

![image]({{site.baseurl}}/assets/mockups/header.png)

Nach der Anpassung sollte der Header also um 16 Pixel an Höhe verlieren und zusätzlich das Logo in einer vereinfachten Form angezeigt werden, konkret ohne den Schriftzug. Zusätzlich müssen natürlich die Icons wie Login/Logout und das Padding angepasst werden, um einen runden Gesamteindruck zu ergeben. 

Dazu war es einerseits notwendig erkennen zu können, wenn die/der User\*in nach unten gescrollt hat. Dazu wurde ein Event-Listener über einen React-Hook hinzugefügt. Der konkrete Code kann [hier](https://gitlab.mediacube.at/photo-master/frontend/-/blob/master/src/helpers/scroll.js "scroll.js in Gitlab") eingesehen werden, wird jedoch hier nicht näher behandelt da darauf nicht der Fokus liegt. Im [Header-File](https://gitlab.mediacube.at/photo-master/frontend/-/blob/master/src/components/header/header.js "header.js in Gitlab") werden diese Daten ausgewertet und anschließend wenn das Minimum von 100 Pixel gescrollt wurde die Variable `smallerHeader` auf `true` gesetzt. Durch die Veränderung dieser Variable verändern sich auch die Variablen `currentStyle` (auf `small`) und `currentLogo` (auf `logo_icon_only`). 

##### Codebeispiel ([header.js](https://gitlab.mediacube.at/photo-master/frontend/-/blob/master/src/components/header/header.js "header.js in Gitlab")): 

```js
const [smallerHeader, setSmallerHeader] = useState(false)

const MIN_SCROLL = 100
const TIMEOUT_DELAY = 400

const currentStyle = smallerHeader ? 'small' : 'large'
const currentLogo = smallerHeader ? logo_icon_only : logo_full_text

useDocumentScrollThrottled(callbackData => {
  const { previousScrollTop, currentScrollTop } = callbackData
  const isMinimumScrolled = currentScrollTop > MIN_SCROLL

  setTimeout(() => {
    setSmallerHeader(isMinimumScrolled)
  }, TIMEOUT_DELAY)
})
```

Durch die Veränderung von `currentStyle` greifen nun andere CSS-Klassen, da `currentStyle` als Klassenname angegeben wird. 

##### Codebeispiel ([header.scss](https://gitlab.mediacube.at/photo-master/frontend/-/blob/master/src/components/header/header.scss "header.scss in Gitlab")):

```scss
.header {
  background-color: $background-white;
  padding: 1rem 0;
  box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.5);
  position: fixed;
  top: 0;
  width: 100%;
  z-index: 1000;
  opacity: 1;
  transition: all .5s ease-out;
  height: 4rem;

  &.small {
    height: 3rem;
    padding: 0.5rem 0;
    opacity: .95;
  }
}
```

Das Ergebnis ist folgendes:

![image]({{site.baseurl}}/assets/screenshots/header_neu.png)

Durch den niedrigeren Header kann nun die letzte Zeile der Fehlermeldung problemlos gelesen werden. Durch die leichte Transparenz wird zudem ersichtlich, dass es weiter oben noch mehr zu lesen gibt. Dadurch ist es für die/den User\*in beim Benutzen der Website jetzt leichter zu erkennen wenn hier etwas nicht funktioniert hat und kann darauf reagieren. 

#### Teaser-Boxen

Die ursprünglichen Teaser-Boxen waren nicht klickbar, wirkten jedoch aufgrund ihres Designs und vor allem aufgrund eines relativ starken Schlagschattens sobald man über sie gehovert hat für viele Personen als wären sie klickbare Elemente.

![image]({{site.baseurl}}/assets/screenshots/boxen_alt.jpg)

Das war für die/den User\*in irritierend, da das erwartete Verhalten nicht dem tatsächlichen Verhalten der Elemente entsprochen hat. Daher wollte das Projektteam ursprünglich den Schlagschatten entfernen und die Elemente offensichtlicher als "nicht klickbar" darstellen. Nach weiteren Überlegungen zu der eigentlichen Funktion der Boxen wurde jedoch entschieden, die Sache anders zu lösen. 

Die Teaser-Boxen sind ursprünglich aus einer Anregung einer Testperson entstanden, die angemerkt hatte, dass sie sich erst "durchklicken müsse, um zu verstehen, was man alles tun kann". Durch die Teaser-Boxen sollen die zentralen Features von *PhotoMaster* übersichtlich präsentiert werden, um so der/dem interessierten Nutzer\*in zu zeigen, was man auf der Website tun kann, was ihre zentralen Features sind. Ein paar Schlagworte alleine erklären jedoch nur sehr wenig und jede Person hat vermutlich eine eigene Vorstellung davon wie die Features funktionieren und wozu sie in der Lage sind. Daher hat das Projektteam entschlossen eine neue Unterseite anzulegen, auf der die zentralen Features ausführlich erklärt werden. Die Teaser-Boxen werden daher zu tatsächlichen Links zu dieser neuen Unterseite. 

Damit ist das Problem jedoch noch nicht gelöst. Die ursprünglichen Teaser-Boxen haben vor allem wie ein Link gewirkt, sobald man über sie gehovert hat. Ohne das zu tun ist ihre Funktion jedoch weiterhin nicht ganz klar indem man sie nur ansieht. Daher ist das neue Ziel die Teaser-Boxen offensichtlich als Link darzustellen. 

Um dieses Problem zu lösen wurde entschieden einen Link-Button unterhalb des Textes zu platzieren, dessen Text "LEARN MORE" lautet und zur neuen Unterseite führt. Aus diesem Grund wurde die Kachel-Darstellung überflüssig bzw. irreführend, da nun nur der Link ein klickbares Element darstellt und nicht mehr die gesamte Kachel. 

Aus diesen Überlegungen enstanden die folgenden drei Varianten an neuen Teaser-Boxen:

1. ![image]({{site.baseurl}}/assets/screenshots/boxen_v1.jpg)
1. ![image]({{site.baseurl}}/assets/screenshots/boxen_v2.jpg)
1. ![image]({{site.baseurl}}/assets/screenshots/boxen_v3.jpg)

In Variante 1 geht der neue Link unter und wirkt - wie zuvor - nicht wie ein klickbares Element. In Variante 3 wird die/der User\*in regelrecht von dem Button "erschlagen", da er so eine kräftige Farbe aufweist. Daher wurde letztendlich Variante 2, der Mittelweg, gewählt. Der Button ist eindeutig als Link erkennbar, wirkt jedoch nicht zu aufdringlich.

Daher ist die neue Darstellung der Teaser-Boxen die folgende:

![image]({{site.baseurl}}/assets/screenshots/boxen_neu.jpg)

##### Codebeispiel ([explanationLink.js](https://gitlab.mediacube.at/photo-master/frontend/-/blob/master/src/components/explanation/explanationLink.js "explanationLink.js in Gitlab")):

``` jsx
<div className={`${baseClasses.padding2} 
  ${baseClasses.flexWrap}
  ${baseClasses.justifyContentCenter}
  ${baseClasses.alignItemsBaseline}
  ${baseClasses.alignContentBetween}`}>
  <Icon style={{ fontSize: 50 }} 
    color={color || 'primary'} 
  />
  <Typography variant="subtitle2">
    {text}
  </Typography>
  <NavLink to={link} style={{ textDecoration: 'none' }} className={baseClasses.marginTop1}>
    <Button variant="outlined" color='primary'>Learn more</Button>
  </NavLink>
</div>
```

#### Neue Unterseite

Wie im vorherigen Kapitel beschrieben, ging es anschließend um die Umsetzung einer neuen Detailseite, welche die Funktionen der Plattform anhand von Texten und Bildern besser erklärt. Da diese Seite eine komplette Neuentwicklung darstellte, wurde zuerst ein Mockup mit dem Balsamiq Tool entwickelt. Dieses Tool wurde bereits für alle bis dato erstellten Mockups der restlichen Seiten und Komponenten verwendet. Ziel der Seite ist es, die Funktionen besser zu erläutern. Angelehnt an die Struktur der Teaser-Boxen auf der Einstiegsseite ist auch die Detailseite nach denselben Aufzählungspunkten gruppiert. Jede Teaser-Box erhält eine eigene "Detail-Box", welche auf der einen Seite einen beschreibenden Text, eine Überschrift und wahlweise einen Button enthält. Auf der anderen Seite der Box ist Platz für einen beliebigen Inhalt. Um die Erscheinung der Box etwas abwechslungsreicher zu gestalten, wird die Position des zusätzlichen Inhalts in jeder Zeile getauscht.

![Boxen Mockup]({{site.baseurl}}/assets/screenshots/boxen_detail_mock.png)  

Die neue React-Komponente wurde mit den Standardkomponenten der MaterialUI Bibliothek umgesetzt (`Grid`, `Card`, `Typography`), die auch für die restliche Applikation verwendet werden. Für die horizontale Ausrichtung, Unterteilung und Breakpoints wird das Gridsystem verwendet. Als Zusatzinhalt kann entweder ein Bild oder jede beliebige andere Komponente eingebettet werden. Beispielsweise könnte man auch die Karte der Einstiegsseite mit speziellen Filtern in der Box einbauen. Somit ist durch die dynamische Struktur der Komponente ebenso eine interaktive Benutzung möglich.

##### Codebeispiel ([image-text-slide.js](https://gitlab.mediacube.at/photo-master/frontend/-/blob/master/src/components/container/image-text-slide.js "image-text-slide in Gitlab")):
```jsx
<Grid container spacing={8} className={classes.container} style={props.reverse ? {flexDirection: 'row-reverse'} : null}>
    <Grid item sm={12} md={6}>
        {props.image && <img className={classes.image} src={props.image} /> || props.children}
    </Grid>
    <Grid item sm={12} md={6}>
    <Card className={baseClasses.padding2}>
        <CardContent>
            <Typography component="h3" variant="h3">
                {props.title}
            </Typography>

            <Typography variant="body2" component="p">
                {!!props.text && props.text}
            </Typography>
        </CardContent>
        {!!props.button && (
              <CardActions>
                  <div className={`${classes.fullWidth} ${baseClasses.justifyContentCenter}`}>
                    {props.button}
                </div>
            </CardActions>
        )}
        </Card>
    </Grid>
</Grid>
```

Im folgenden Screenshot wird die fertige Implementierung dargestellt. Die Grafiken und Texte dienen derzeit noch als Platzhalter. Die Illustrationen von unDraw.co stehen unter einer [Open-Source Lizenz](https://undraw.co/license) und können frei verwendet werden[^4]. 

![Boxen auf Detailseite: Implementierung]({{site.baseurl}}/assets/screenshots/boxen_detail.png)  



# Quellen
[^1]: BAT Stiftung für Zukunftsfragen. 2013. [Was machen Sie in Ihrem Urlaub?](https://de.statista.com/statistik/daten/studie/263557/umfrage/umfrage-zur-urlaubsgestaltung-der-deutschen/), Zugegriffen am 17.6.2020

[^2]: Browne, Sarah. 2013. [Images are everything: How MR can mine the visual side of social media](https://www.quirks.com/articles/images-are-everything-how-mr-can-mine-the-visual-side-of-social-media), Zuegriffen am 17.6.2020

[^3]: Brandt, Mathias. 2019. [Die Opfer des Smartphone-Booms](https://de.statista.com/infografik/1958/die-opfer-des-smartphone-booms/), Zugegriffen am 17.6.2020

[^4]: Undraw.co. 2020. [Undraw - Open Source Illustrations For Any Idea](https://undraw.co/), Zugegriffen am 18.6.2020

[^5]: Schneider, Kristina. 2020. [Consistent, engaging and delightful UI](https://wiki.mediacube.at/wiki/images/3/3c/2020-SS_UI-CSS-Workshop_Kristina-Schneider-2.Sitzung.pdf), Zugegriffen am 18.6.2020
