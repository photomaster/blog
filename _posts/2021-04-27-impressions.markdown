---
layout: post
title:  "Impressions"
date:   2021-04-27 16:30:00 +0200
categories: mmt summary
---
**In diesem Beitrag wird ein visueller Eindruck von PhotoMaster gegeben.**

## Startseite

![image]({{site.baseurl}}/assets/impressions/startseite.png)

So sieht die Startseite von PhotoMaster aus, auf welcher die User\*innen zunächst von einer Karte mit den Fotos in ihrer Nähe begrüßt werden. Darunter befindet sich eine kurze Einführung in die Kernfeatures der Website sowie Empfehlungen von Fotos, die sich in der Nähe der/des User\*in befinden. 

## Features

![image]({{site.baseurl}}/assets/impressions/features.png)

Die Kernfeatures von PhotoMaster werden auch auf einer eigenen Übersichtsseite vorgestellt, erklärt und verlinkt. So können sich neue User\*innen direkt in ihre liebsten Teile der Website stürzen!

## Foto-Upload

![image]({{site.baseurl}}/assets/impressions/upload.png)

Der Foto-Upload besteht aus 3 Schritten, wobei hier exemplarisch der zweite zu sehen ist (die restlichen Ansichten können [hier](https://drive.google.com/drive/folders/1WL7CqHY4a25dJw6KQdmFbrnsWL0xL224?usp=sharing) angesehen werden). In diesem Schritt können die User\*innen mithilfe der Karte und einer Suchleiste den Ort des Fotos definieren. Zudem können zentrale Informationen wie Titel, Datum und eine Beschreibung hinzufügt werden. 

## Foto-Details

![image]({{site.baseurl}}/assets/impressions/foto.png)

Pro Foto existiert eine Seite, auf der sämtliche Informationen übersichtlich dargestellt werden. Dazu zählen unter anderem der Eigentümer, die vergebenen Tags, das vorherrschende Wetter, Kameraeinstellungen und Empfehlungen zu nahen und optisch sowie inhaltlich ähnlichen Fotos. 

## Foto bearbeiten

![image]({{site.baseurl}}/assets/impressions/foto_edit.png)

Die Informationen zu einem Foto können natürlich jederzeit vom Besitzer aktualisiert werden. 

## Persönliches Profil

![image]({{site.baseurl}}/assets/impressions/profile.png)

Jede/r User\*in besitzt zudem eine Profil-Seite, auf der er/sie sich kurz vorstellen kann sowie die beliebtesten Fotos angezeigt werden. Das Banner-Bild im oberen Bereich kann ebenfalls individuell festgelegt werden. 

## Suche

![image]({{site.baseurl}}/assets/impressions/suche.png)

Die Suche, die über das Such-Icon im Header erreicht werden kann, bietet verschiedene Filter, die auf Fotograf\*innen zugeschnitten sind. Neben einem üblichen Keyword können die Ergebnisse so auch anhand von Kameramarke/-modell, Jahreszeit, Wetterlage sowie Temperaturbereich eingegrenzt werden. 

## Mobile

![image]({{site.baseurl}}/assets/impressions/mobile.png)

Natürlich ist die Website auch für mobile Geräte optimiert und kann auch auf diesen im vollen Umfang genutzt werden. 

## Weitere Impressionen

Weitere Bilder von PhotoMaster können in Google-Drive aufgerufen werden: [Alle Bilder in Google Drive](https://drive.google.com/drive/folders/1WL7CqHY4a25dJw6KQdmFbrnsWL0xL224?usp=sharing)
