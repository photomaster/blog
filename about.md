---
layout: page
title: About
permalink: /about/
---

Photomaster ist eine Web-Applikation, welche Fotograf*innen bei der Auswahl von Locations und Motiven helfen soll. Das Projekt wird von Lisa Koller und Lukas Gruber aus dem Masterstudiengang Multimediatechnology an der FH Salzburg umgesetzt. 

Du kannst den Quellcode dieses Projekts und all seiner Module im Gitlab der FH Salzburg finden:
{% include icon-gitlab.html username=site.gitlab_username %} /